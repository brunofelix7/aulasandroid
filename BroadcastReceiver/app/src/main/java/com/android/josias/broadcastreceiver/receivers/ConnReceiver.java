package com.android.josias.broadcastreceiver.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class ConnReceiver extends BroadcastReceiver {
    public ConnReceiver() {
    }

    /*
    *   O SEGREDO ESTÁ NO ANDROID MANIFEST
    *   intent-filter
    *   permissões
    */
    @Override
    public void onReceive(Context context, Intent intent) {

        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();



        if (isConnected){
            Log.i("teste", "CONECTADO! - Wifi? " + " - " + activeNetwork.getTypeName());
        }else{
            Log.i("teste", "DESCONECTADO!");
        }
    }

}

