package com.android.josias.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;


public class MainActivity extends ActionBarActivity {

    private BroadcastReceiver br;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void startMyBR(View v){
        Intent intent = new Intent();
        intent.setAction("com.android.josias.broadcastreceiver.MYRECEIVER");
        sendBroadcast(intent);
    }
}

