package br.com.uther.httpcliente_okhttp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import br.com.uther.httpcliente_okhttp.core.AuthController;
import br.com.uther.httpcliente_okhttp.core.Constants;
import br.com.uther.httpcliente_okhttp.model.AuthResult;
import br.com.uther.httpcliente_okhttp.model.MapsDirection;
import br.com.uther.httpcliente_okhttp.model.User;

public class MainActivity extends AppCompatActivity {

    private String jsonString = "{\"Id\":123,\"email\":\"joaodasilva@mail.com\",\"nome\":\"João da Silva\"}";
    private String responseJSON;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ExampleOkHttp().execute();
            }
        });
    }

    public void okHttpPost(View v) {
        Intent i = new Intent(this, OkHttpPostActivity.class);
        startActivity(i);
    }

    public void parseUserToJSON(View v){
        User user = new User();
        user.setNome("João da Silva");
        user.setEmail("joaodasilva@mail.com");

        //código que faz o trabalho ;-)
        Gson gson = new Gson();
        String userJSONString = gson.toJson(user);

        //Para ver o resultado no Logcat
        Log.d("Gson", "User JSON String: "+userJSONString);

        Snackbar snackbar = Snackbar
                .make(v, userJSONString, Snackbar.LENGTH_LONG);

        snackbar.show();

    }

    public void loadUserFromJSON(View v) {
        Gson gson = new Gson();
        User user = gson.fromJson(jsonString, User.class);

        Log.d("Gson", "User infos: " + user.getNome() + " - " + user.getEmail());

        Snackbar snackbar = Snackbar
                .make(v, user.toString(), Snackbar.LENGTH_LONG);

        snackbar.show();
    }

    private class ExampleOkHttp extends AsyncTask<Void, Void, Response>{

        OkHttpClient client;
        Request request;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            client = new OkHttpClient();

            request = new Request.Builder()
                    .url(Constants.URL_SERVICE3)
                    .build();
        }
        @Override
        protected Response doInBackground(Void... params) {
            try {
                Response response = client.newCall(request).execute();
                return response;
            }catch (IOException e){
                return null;
            }
        }
        @Override
        protected void onPostExecute(Response response) {
            try {
                if (response.message().equals("OK")){
                    responseJSON = AuthController.getInstance().loadAuthFromJSON(response.body().string());
                }
            }catch (IOException e){
                responseJSON = "OPS - Fail connection";
            }

            Toast.makeText(MainActivity.this, responseJSON,Toast.LENGTH_LONG).show();
        }
    }
}

