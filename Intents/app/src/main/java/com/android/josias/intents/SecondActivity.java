package com.android.josias.intents;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.josias.intents.core.ControllerMainActivity;


public class SecondActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

//        if (getIntent().getExtras() != null){
//            String tagName = getIntent().getExtras().getString("tagName");
//            Toast.makeText(this, tagName, Toast.LENGTH_SHORT).show();
//        }

        //Descomente para testar o transfer2
        String value = ControllerMainActivity.getInstance().getValue();
        Toast.makeText(this, value, Toast.LENGTH_SHORT).show();

    }

    public void tercScreen(View v){
        Intent i = new Intent(this, TercActivity.class);
        startActivity(i);
        //Não vai voltar para a MainActivity :)
        //finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_second, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
