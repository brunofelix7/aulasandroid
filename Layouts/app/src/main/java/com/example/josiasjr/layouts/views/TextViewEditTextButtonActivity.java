package com.example.josiasjr.layouts.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.josiasjr.layouts.R;

public class TextViewEditTextButtonActivity extends AppCompatActivity {

    private TextView tvTitulo;
    private EditText etText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_ed_button);

        tvTitulo = (TextView)findViewById(R.id.tvTitulo);
        etText = (EditText)findViewById(R.id.etText);
    }

    public void buttonClick(View v){
        String text = etText.getText().toString();
        tvTitulo.setText(text);
        etText.setText("");
    }

}
