package com.example.josiasjr.layouts.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.josiasjr.layouts.R;

public class CheckBoxActivity extends AppCompatActivity {

    private TextView tvTitulo;
    private EditText etText;
    private CheckBox cbLock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_box);

        tvTitulo = (TextView)findViewById(R.id.tvTitulo);
        etText = (EditText)findViewById(R.id.etText);
        cbLock = (CheckBox)findViewById(R.id.cbLock);
    }

    public void buttonClick(View v){
        if (!cbLock.isChecked()){
            String text = etText.getText().toString();
            tvTitulo.setText(text);
            etText.setText("");
        }
    }

    public void alertCheck(View v){
        if (cbLock.isChecked()){
            Toast.makeText(this, "Você não vai conseguir! :)",
                    Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Liberado! :P",
                    Toast.LENGTH_SHORT).show();
        }
    }

}