package com.example.josiasjr.layouts;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.josiasjr.layouts.views.CheckBoxActivity;
import com.example.josiasjr.layouts.views.DateTimePickerActivity;
import com.example.josiasjr.layouts.views.FloatButtonActivity;
import com.example.josiasjr.layouts.views.ImageViewActivity;
import com.example.josiasjr.layouts.views.RadioButtonActivity;
import com.example.josiasjr.layouts.views.RatingBarActivity;
import com.example.josiasjr.layouts.views.SpinnerActivity;
import com.example.josiasjr.layouts.views.TextViewEditTextButtonActivity;
import com.example.josiasjr.layouts.views.WebViewActivity;


public class ViewsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_views);
    }

    public void openFloatButton (View v){
        Intent i = new Intent(this, FloatButtonActivity.class);
        startActivity(i);
    }

    public void openTvButton (View v){
        Intent i = new Intent(this, TextViewEditTextButtonActivity.class);
        startActivity(i);
    }

    public void openCheckBox (View v){
        Intent i = new Intent(this, CheckBoxActivity.class);
        startActivity(i);
    }

    public void openRadioButton (View v){
        Intent i = new Intent(this, RadioButtonActivity.class);
        startActivity(i);
    }

    public void openIV (View v){
        Intent i = new Intent(this, ImageViewActivity.class);
        startActivity(i);
    }

    public void openSpinner (View v){
        Intent i = new Intent(this, SpinnerActivity.class);
        startActivity(i);
    }

    public void openRB (View v){
        Intent i = new Intent(this, RatingBarActivity.class);
        startActivity(i);
    }

    public void openWebView (View v){
        Intent i = new Intent(this, WebViewActivity.class);
        startActivity(i);
    }

    public void openDTPicker (View v){
        Intent i = new Intent(this, DateTimePickerActivity.class);
        startActivity(i);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_views, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
