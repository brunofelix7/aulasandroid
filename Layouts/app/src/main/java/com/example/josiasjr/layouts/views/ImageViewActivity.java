package com.example.josiasjr.layouts.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.example.josiasjr.layouts.R;

public class ImageViewActivity extends AppCompatActivity {

    private ImageView ivImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);

        ivImg = (ImageView)findViewById(R.id.ivImg);
    }

    public void showImg(View v){
        ivImg.setVisibility(View.VISIBLE);
    }

    //GONE REMOVE O COMPONENTE DO LAYOUT - MAS MANTEM A REFERENCIA
    //INVISIBLE - ESCONDE E MANTER O ESPAÇO DA VIEW
    public void hideImg(View v){
        ivImg.setVisibility(View.GONE);
    }

    public void imgFitXY(View v){
        ivImg.setScaleType(ImageView.ScaleType.FIT_XY);
    }

    public void imgFitStart(View v){
        ivImg.setScaleType(ImageView.ScaleType.FIT_START);
    }

    public void imgFitEnd(View v){
        ivImg.setScaleType(ImageView.ScaleType.FIT_END);
    }

    public void imgCenterInside(View v){
        ivImg.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
    }

    public void imgCenter(View v){
        ivImg.setScaleType(ImageView.ScaleType.CENTER);
    }

    public void imgCenterCrop(View v){
        ivImg.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }

    public void imgMatrix(View v){
        ivImg.setScaleType(ImageView.ScaleType.MATRIX);
    }

}
