package com.example.josiasjr.layouts.core;

import com.example.josiasjr.layouts.R;

/**
 * Created by JosiasJr on 19/02/2016.
 */
public class ControllerCoordinator {

    private static ControllerCoordinator INSTANCE = null;

    private int layoutConfig;

    private ControllerCoordinator(){}

    public static ControllerCoordinator getInstance ()
    {
        if (INSTANCE == null)
            INSTANCE = new ControllerCoordinator();
        return INSTANCE;
    }

    public int getLayoutConfig() {
        return layoutConfig;
    }

    public void setLayoutConfig(int layoutConfig) {
        this.layoutConfig = layoutConfig;
    }
}
