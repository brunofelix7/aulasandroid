package com.android.josias.googlemaps;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {

        LatLng latlng = new LatLng(-23.55668, -46.65896);
        mMap.addMarker(createMarker(latlng, "Nome da rua", "Descrição"));
        configMap(mMap, latlng, GoogleMap.MAP_TYPE_HYBRID);
    }

    private void configMap(GoogleMap map, LatLng latlng, int type){
        map.setMapType(type);
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 17.0f));
    }

    private MarkerOptions createMarker(LatLng latlng, String titulo, String snippet){
        MarkerOptions marker = new MarkerOptions();
        marker.position(latlng);
        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher));
        marker.title(titulo);
        marker.snippet(snippet);

        return marker;
    }
}
